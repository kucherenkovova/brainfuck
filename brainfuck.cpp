#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>

char tape[30000] = {0};
int tapeIndex = 0;

void brainfuck(std::string code);

int main() {
    std::string code;
    std::cin>>code;
    while(code[0] != 'e') {
        brainfuck(code);
        std::cin>>code;
    }
    return 0;
}
/*This code prints "Hello, world!"
++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.
*/

void brainfuck(std::string code) {
    int length = code.length();
    for(int i = 0; i < length; i++) {
        switch(code[i]) {
        case '[': {
            int tmpIndex = i;
            while(code[tmpIndex] != ']')
                tmpIndex++;
            while(tape[tapeIndex]) {
                brainfuck(code.substr(i + 1, tmpIndex - i));
            }
            i = tmpIndex;
        }
            break;
        case '.':
            putchar(tape[tapeIndex]);
            break;
        case ',':
            getchar();
            tape[tapeIndex] = getchar();
            break;
        case '>':
            ++tapeIndex;
            break;
        case '<':
            --tapeIndex;
            break;
        case '+':
            ++tape[tapeIndex];
            break;
        case '-':
            --tape[tapeIndex];
            break;
        default:
            break;
        }
    }
}
